﻿using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace Microsoft.eShopWeb.Infrastructure.Identity
{
    public class AppIdentityDbContextSeed
    {
        public static async Task SeedAsync(UserManager<ApplicationUser> userManager)
        {
            var defaultUser = new ApplicationUser { UserName = "HJPULIDO@GAFI.COM.MX", Email = "HJPULIDO@GAFI.COM.MX" };
            await userManager.CreateAsync(defaultUser, "12345678910");
        }
    }
}
